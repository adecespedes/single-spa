const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: ['buefy'],
  configureWebpack: {
    output: {
      libraryTarget: 'system',
    },
  }
})
